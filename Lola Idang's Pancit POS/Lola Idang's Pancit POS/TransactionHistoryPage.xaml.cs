﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LI_POS
{
    /// <summary>
    /// Interaction logic for TransactionHistoryPage.xaml
    /// </summary>
    public partial class TransactionHistoryPage : UserControl
    {
        ObservableCollection<Transaction> transactionList = new ObservableCollection<Transaction>();
        ObservableCollection<Transaction> searchedTransactions = new ObservableCollection<Transaction>();
        ObservableCollection<OrderedProduct> transactionItems = new ObservableCollection<OrderedProduct>();

        Transaction selectedTransaction = null;

        public TransactionHistoryPage()
        {
            InitializeComponent();

            //using (LIPOSDbContext db = new LIPOSDbContext())
            //{
            //    List<Transaction> allTransactions = (from transactions in db.Transactions select transactions).OrderBy(k => k.TransactionID).ToList();
            //    foreach (Transaction t in allTransactions)
            //    {
            //        transactionList.Add(t);
            //    }
            //}

            //foreach (Transaction t in transactionList)
            //{
            //    searchedTransactions.Add(t);
            //}

            DataGridTransactionHistory.ItemsSource = searchedTransactions;
            DataGridTransactionDetails.ItemsSource = transactionItems;

            
            dpDate.DisplayDate = DateTime.Today;
            dpDate.Text = DateTime.Today.ToString("dd/mm/yyyy");

        }

        private void renameTransactionHistoryColumnHeaders(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            // sample code
            switch (e.Column.Header.ToString())
            {
                case "TransactionID":
                    e.Column.Header = "Transaction ID";
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "TransactionDate":
                    e.Column.Header = "Date";
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "LoggedInEmployee":
                    e.Column.Header = "Cashier";
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "TotalPrice":
                    e.Column.Header = "Amount Due";
                    e.Column.Visibility = Visibility.Visible;
                    break;
                default:
                    e.Column.Visibility = Visibility.Hidden;
                    break;
            }
        }

        private void renameTransactionDetailsColumnHeaders(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (e.Column.Header.ToString())
            {
                case "Name":
                    e.Column.Header = "Item Description";
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "Quantity":
                    e.Column.DisplayIndex = 0;
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "Price":
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "SubtotalPrice":
                    e.Column.Header = "Subtotal";
                    e.Column.Visibility = Visibility.Visible;
                    break;
                default:
                    e.Column.Visibility = Visibility.Hidden;
                    break;
            }
        }
        

        private void showDetails(object sender, MouseButtonEventArgs e)
        {
            selectedTransaction = (Transaction)DataGridTransactionHistory.SelectedItem;

            lblDate.Content = String.Format("{0} {1}", selectedTransaction.TransactionDate, selectedTransaction.TransactionTime);
            lblCashier.Content = selectedTransaction.LoggedInEmployee;
            lblAmountDue.Content = selectedTransaction.TotalPrice;
            if (selectedTransaction.SeniorCitizenID != -1)
            {
                using (LIPOSDbContext db = new LIPOSDbContext())
                {
                    SeniorCitizen sc = (from senior in db.SeniorCitizens select senior)
                        .Where(s => s.SeniorCitizenID == selectedTransaction.SeniorCitizenID)
                        .FirstOrDefault();
                    lblSCTransaction.Content = String.Format("{0} {1} | SC # {2}", sc.FirstName, sc.LastName, sc.SCNumber);
                }
            }
            else
            {
                lblSCTransaction.Content = "N/A";
            }

            // Reset order details datagrid
            transactionItems.Clear();
            foreach(OrderedProduct p in Reports.decodeOrders(selectedTransaction)){
                transactionItems.Add(p);
            }


        }

        private void toDashboardPage(object sender, RoutedEventArgs e)
        {
            Session.LoggedInWindow.ShowDashboardPage();
        }

        private void searchTransactionsByDate(object sender, SelectionChangedEventArgs e)
        {
            chkAllTransactions.IsChecked = false;
            searchedTransactions.Clear();
            String selectedDate = ((DateTime)dpDate.SelectedDate).ToString("MM/dd/yyyy");


            using (LIPOSDbContext db = new LIPOSDbContext())
            {
                foreach (Transaction t in (from transaction in db.Transactions where transaction.TransactionDate == selectedDate select transaction).ToList())
                {
                    searchedTransactions.Add(t);
                }
            }
        }

        private void getAllTransactions(object sender, RoutedEventArgs e)
        {
            /*  Displays all transactions when 'All Transactions' Checkbox is checked. */
            searchedTransactions.Clear();
            using (LIPOSDbContext db = new LIPOSDbContext())
            {
                List<Transaction> allTransactions = (from transactions in db.Transactions select transactions).OrderBy(k => k.TransactionID).ToList();
                foreach (Transaction t in allTransactions)
                {
                    searchedTransactions.Add(t);
                }
            }
        }

        private void triggerSearchTransactionsByDate(object sender, RoutedEventArgs e)
        {
            /* Triggers the searchTransactionsByDate
             * when 'All Transactions' checkbox is unchecked'
             */

            searchTransactionsByDate(null, null);
        }

        private void printReceipt(object sender, RoutedEventArgs e)
        {
            if (selectedTransaction != null)
            {
                Reports.PrintReceipt(selectedTransaction);
            }
        }
    }
}
