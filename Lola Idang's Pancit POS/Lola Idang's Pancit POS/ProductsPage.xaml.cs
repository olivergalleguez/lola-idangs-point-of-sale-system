﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LI_POS
{
    /// <summary>
    /// Interaction logic for ProductsPage.xaml
    /// </summary>
    public partial class ProductsPage : UserControl
    {

        private ObservableCollection<Product> productsList = new ObservableCollection<Product>();


        public ProductsPage()
        {
            InitializeComponent();

            
            using (LIPOSDbContext db = new LIPOSDbContext())
            {
                List<Product> prodlist = (from product in db.Products select product).OrderBy(p => p.ProductID).ToList();
                
                foreach (Product prod in prodlist)
                {
                    productsList.Add(prod);
                }
            }

            if (productsList != null)
            {
                DataGridProductList.ItemsSource = productsList;
            }
            
        }

        private void renameColumnHeaders(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            // sample code
            switch (e.Column.Header.ToString())
            {
                case "AssetId":
                    e.Column.Header = "Asset ID";
                    e.Column.Visibility = Visibility.Hidden;
                    break;
                  
            }
        }

        private void showNewProductForm(object sender, RoutedEventArgs e)
        {
            ProductForm prodform = new ProductForm(FormMode.New, null);

            prodform.ShowDialog();

            // add recent product to list (if any)
            if (Session.RecentlyAddedProduct != null)
            {
                productsList.Add(Session.RecentlyAddedProduct);
                Session.RecentlyAddedProduct = null;
            }
        }

        //private void showEditProductForm(object sender, RoutedEventArgs e)
        //{            
        //    Product selectedProduct = (Product)DataGridProductList.SelectedItem;
        //    ProductForm prodform = new ProductForm(FormMode.Edit, selectedProduct);
        //    prodform.ShowDialog();
        //}

        private void showDetails(object sender, MouseButtonEventArgs e)
        {
            Product selectedProduct = (Product)DataGridProductList.SelectedItem;
            ProductForm prodform = new ProductForm(FormMode.View, selectedProduct);
            prodform.ShowDialog();
            if (Session.RecentlyModifiedProduct != null)
            {
                Product edit = (from toEdit in productsList where toEdit.ProductID == Session.RecentlyModifiedProduct.ProductID select toEdit).FirstOrDefault();
                if (edit != null)
                {
                    productsList[productsList.IndexOf(edit)] = Session.RecentlyModifiedProduct;
                }
                
                Session.RecentlyModifiedProduct = null;
            }

            if (Session.RecentlyDeletedProduct != -1)
            {
                Product toDelete = productsList.Where(x => x.ProductID == Session.RecentlyDeletedProduct).FirstOrDefault();
                productsList.Remove(toDelete);
            }
        }

        private void toDashboardPage(object sender, RoutedEventArgs e)
        {
            Session.LoggedInWindow.ShowDashboardPage();
        }



    }
}
