﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Web.Script.Serialization;

namespace LI_POS
{
    public class Reports
    {
        public static void PrintReceipt(Transaction transaction)
        {
            PrintDialog printDialog = new PrintDialog();

            if (printDialog.ShowDialog().GetValueOrDefault())
            {
                
                FlowDocument flowDocument = new FlowDocument();
                flowDocument.ColumnWidth = 1000;


                List<OrderedProduct> orders = decodeOrders(transaction);
                Paragraph p = new Paragraph()
                {
                    Margin = new System.Windows.Thickness(0),
                    TextAlignment = System.Windows.TextAlignment.Left,
                    FontFamily = new System.Windows.Media.FontFamily("Consolas"),
                    FontSize = 10
                };

                // Header
                p.Inlines.Add("         LOLA IDANG'S ORIGINAL PANCIT MALABON");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("       POINT OF SALE SYSTEM SAMPLE TEST RECEIPT");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("BETTER LIVING SUBDIVISION PARANAQUE CITY PHILIPPINES");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("             VAT Reg TIN 218-897-998-908");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("             ACCR#: 890-000987788-887877");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("             Permit#: 0657-989-123243-431");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add(String.Format("{0} {1}", transaction.TransactionDate, transaction.TransactionTime));
                p.Inlines.Add(new LineBreak());

                // Transaction Details
                p.Inlines.Add("-----------------------------------------------------");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("Cashier: " + transaction.LoggedInEmployee);
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("Receipt Number: " + transaction.TransactionID);
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("-----------------------------------------------------");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("Qty\tDescription\tUnit Price\tSubtotal");
                foreach (OrderedProduct order in orders)
                {
                    p.Inlines.Add(new LineBreak());
                    p.Inlines.Add(String.Format("{0}\t{1}\t@ {2}\t\t{3}", order.Quantity, order.NameShortcut, order.Price, order.SubtotalPrice));
                    flowDocument.Blocks.Add(p);
                    
                }

                // Summary
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add(String.Format("NET\t\t\t\t\t{0}", transaction.Net));
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add(String.Format("12% VAT\t\t\t\t\t{0}", transaction.VAT));
                p.Inlines.Add(new LineBreak());
                if (transaction.SeniorCitizenID != -1)
                {
                    decimal scdiscount = transaction.Net + transaction.VAT - transaction.TotalPrice;
                    p.Inlines.Add(String.Format("SENIOR CITIZEN DISCOUNT\t\t{0}", scdiscount));
                    p.Inlines.Add(new LineBreak());
                }
                p.Inlines.Add(String.Format("AMOUNT DUE\t\t\t\t{0}", transaction.TotalPrice));
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add(String.Format("CASH\t\t\t\t\t{0}", transaction.AmountReceived));
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add(String.Format("CHANGE\t\t\t\t\t{0}", transaction.AmountReceived - transaction.TotalPrice));
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add(String.Format("TOTAL ITEMS\t\t\t\t{0}", orders.Count()));
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add(new LineBreak());

                // Footer
                p.Inlines.Add("-----------------------------------------------------");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("         THIS SERVES AS YOUR OFFICIAL RECEIPT");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("                  12% VAT INCLUDED");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("             THANK YOU, PLEASE COME AGAIN");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("FOR QUESTIONS AND FEEDBACK PLEASE CONTACT OR EMAIL US");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("             feedback@lolaidangspancit.com");
                p.Inlines.Add(new LineBreak());
                p.Inlines.Add("                 HAVE A GREAT DAY :)");
                p.Inlines.Add(new LineBreak());



                

                DocumentPaginator paginator = ((IDocumentPaginatorSource)flowDocument).DocumentPaginator;
                printDialog.PrintDocument(paginator,"Print Receipt");
                
            }
        }

    

        public static List<OrderedProduct> decodeOrders(Transaction t)
        {
            List<OrderedProduct> orders = new List<OrderedProduct>();

            JavaScriptSerializer jss = new JavaScriptSerializer();
            orders = jss.Deserialize<List<OrderedProduct>>(t.Details);

            return orders;

        }

  



    }
}
