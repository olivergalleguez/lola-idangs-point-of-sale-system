﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace LI_POS
{
    public static class Session
    {
        // There can only be one:

        public static User ActiveUser { get; set; }

        public static String PreviousPage { get; set; }

        public static LoggedInWindow LoggedInWindow = new LoggedInWindow();

        public static Product RecentlyAddedProduct { get; set; }

        public static Product RecentlyModifiedProduct { get; set; }

        public static int RecentlyDeletedProduct { get; set; }
    }
}
