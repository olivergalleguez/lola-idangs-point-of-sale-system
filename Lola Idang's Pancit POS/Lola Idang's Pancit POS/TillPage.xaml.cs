﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Web.Script.Serialization;

namespace LI_POS
{
    /// <summary>
    /// Interaction logic for TillPage.xaml
    /// </summary>
    public partial class TillPage : UserControl
    {
        private ObservableCollection<Product> productsList = new ObservableCollection<Product>();
        private ObservableCollection<Product> productsListSearchResults;
        private ObservableCollection<OrderedProduct> ordersList = new ObservableCollection<OrderedProduct>();

        private decimal totalAmount = 0, totalCapitalAmount = 0;
        
        public TillPage()
        {
            InitializeComponent();

            using (LIPOSDbContext db = new LIPOSDbContext())
            {
                List<Product> prodlist = (from product in db.Products select product).OrderBy(p => p.ProductID).ToList();

                foreach (Product prod in prodlist)
                {
                    productsList.Add(prod);
                }

            }

            if (productsList != null)
            {
                productsListSearchResults = new ObservableCollection<Product>(productsList);
                DataGridProductList.ItemsSource = productsListSearchResults;
            }

            DataGridOrdersList.ItemsSource = ordersList;

            ordersList.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(calculateTotal);
        }

        private void renameProductListColumnHeaders(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            // sample code
            switch (e.Column.Header.ToString())
            {
                case "Price":
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "Name":
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "Image":
                    e.Column.Visibility = Visibility.Visible;
                    e.Column.DisplayIndex = 0;
                    break;
                default:
                    e.Column.Visibility = Visibility.Hidden;
                    break;                   

            }
        }
        private void toDashboardPage(object sender, RoutedEventArgs e)
        {
            Session.LoggedInWindow.ShowDashboardPage();
        }

        private void addProductToOrder(object sender, MouseButtonEventArgs e)
        {
            int quantity = 1;
            try
            {
                quantity = QuantityInputBox.Show();
                if (quantity == -1) { throw new ArgumentOutOfRangeException(); }
                Product selected = (Product)DataGridProductList.SelectedItem;
                OrderedProduct newOrderedItem = new OrderedProduct();

                newOrderedItem.ProductID = selected.ProductID;
                newOrderedItem.Quantity = quantity;
                newOrderedItem.Price = selected.Price;
                newOrderedItem.CapitalPrice = selected.CapitalPrice;
                newOrderedItem.Name = selected.Name;
                newOrderedItem.NameShortcut = selected.NameShortcut;
                newOrderedItem.SubtotalPrice = selected.Price * quantity;
                newOrderedItem.SubtotalCapitalPrice = selected.CapitalPrice * quantity;

                OrderedProduct test = ordersList.Where(item => item.ProductID == selected.ProductID).FirstOrDefault();
                if (test == null)
                {
                    ordersList.Add(newOrderedItem);
                }
            }
            catch (FormatException f)
            {
                MessageBox.Show("Please enter an integer.", "Invalid input", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (ArgumentOutOfRangeException a)
            {
                // do nothing
            }
            finally
            {
                /* Ariel happened to me
                 * One wash clean
                 * Sa labada
                 * for just
                 * 7.50 :D
                 */                
            }
            
        }

        private void removeProductToOrder(object sender, MouseButtonEventArgs e)
        {
            OrderedProduct toRemove = (OrderedProduct)this.DataGridOrdersList.SelectedItem;
            ordersList.Remove(toRemove);

        }

        private void renameOrderListColumnHeaders(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            switch (e.Column.Header.ToString())
            {
                case "Price":
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "Name":
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "Quantity":
                    e.Column.Visibility = Visibility.Visible;
                    break;
                case "SubtotalPrice":
                    e.Column.Visibility = Visibility.Visible;
                    e.Column.Header = "Subtotal";
                    break;
                default:
                    e.Column.Visibility = Visibility.Hidden;
                    break;

            }
        }

        private void calculateTotal(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs args)
        {
            // reset values
            int totalItems = 0;
            totalAmount = 0;
            totalCapitalAmount = 0;

            foreach (OrderedProduct o in ordersList){
                totalItems += o.Quantity;
                totalAmount += o.SubtotalPrice;
                totalCapitalAmount += o.SubtotalCapitalPrice;
            }

            if (chkSCDiscount.IsChecked == true)
            {
                totalAmount = totalAmount * (decimal) 0.77;
            }

            lblTotalAmount.Content = String.Format("Php {0}", totalAmount.ToString());
            lblTotalItems.Content = String.Format("{0} Item(s)", totalItems.ToString());
            
        }

        private void searchProductList(object sender, TextChangedEventArgs e)
        {
            String query = txtSearch.Text.ToLower().Trim();
            List<Product> searchResults = productsList.Where(product => product.Name.ToLower().Contains(query)).ToList();
            
            productsListSearchResults.Clear();
            foreach (Product p in searchResults)
            {
                productsListSearchResults.Add(p);
            }

            if (productsListSearchResults.Count != 0)
            {
                DataGridProductList.SelectedItem =  DataGridProductList.Items[0];
            }

            if (query == "")
            {
                DataGridProductList.SelectedItem = null;
            }
        }

        private void checkOut(object sender, RoutedEventArgs e)
        {
            try
            {
                Decimal payment = Decimal.Parse(txtAmountTendered.Text);
                SeniorCitizen SCCustomer;
                if (payment < totalAmount)
                {
                    throw new ArgumentOutOfRangeException();
                }

                if (chkSCDiscount.IsChecked == true)
                {
                    SCCustomer = SeniorCitizenForm.GetSC();
                    if (SCCustomer == null) return; // cancelled by alt+f4
                }
                else
                {
                    SCCustomer = null;
                }

                saveTransaction(ordersList, payment, SCCustomer);

                MessageBox.Show("Transaction complete.", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                                
                ordersList.Clear();
                txtAmountTendered.Clear();

            }
            catch (FormatException f)
            {
                MessageBox.Show("Please enter a valid value", "Invalid Format", MessageBoxButton.OK, MessageBoxImage.Error);
                txtAmountTendered.Focus();
            }
            catch (ArgumentOutOfRangeException a)
            {
                MessageBox.Show("Insufficient amount. Please enter a value higher than the total amount", "Insufficient Amount", MessageBoxButton.OK, MessageBoxImage.Warning);
                txtAmountTendered.Focus();
            }

        }

        private void enterKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Product product = (Product)DataGridProductList.SelectedItem;

                if (product != null)
                {
                    addProductToOrder(null, null);
                }
            }
        }

        public void saveTransaction(ObservableCollection<OrderedProduct> OrderedProductList, decimal amountTendered, SeniorCitizen seniorCitizen)
        {
            // net = Total Price - VAT
            // totalPrice = Total Price minus Senior Citizen discount
            // VAT = Total Price * 0.12
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (LIPOSDbContext db = new LIPOSDbContext())
            {
                decimal totalCapitalPrice = 0, totalPrice = 0;
                foreach(OrderedProduct op in OrderedProductList){
                    totalCapitalPrice += op.SubtotalCapitalPrice;
                    totalPrice += op.SubtotalPrice;
                }

                decimal vat = totalPrice * (decimal)0.12;
                decimal net = totalPrice - vat;
                decimal seniorCitizenMultiplier = (seniorCitizen != null) ? (decimal) 0.77 : 1;

                string jsonTransaction = serializer.Serialize(OrderedProductList);
                Transaction transaction = new Transaction()
                {
                    TransactionDate = DateTime.Now.ToString("MM/dd/yyyy"),
                    TransactionTime = DateTime.Now.ToString("HH:mm"),
                    LoggedInEmployee = String.Format("{0} {1}", Session.ActiveUser.FirstName, Session.ActiveUser.LastName),                    
                    Details = jsonTransaction,
                    SeniorCitizenID = (seniorCitizen != null) ? seniorCitizen.SeniorCitizenID : -1,
                    TotalCapitalPrice = totalCapitalPrice,
                    TotalPrice =  totalPrice * seniorCitizenMultiplier,
                    VAT = vat,
                    Net = net,
                    AmountReceived = amountTendered                    
                };

                db.Transactions.Add(transaction);
                db.SaveChanges();

                Reports.PrintReceipt(transaction);
            }

        }

        private void removeSC(object sender, RoutedEventArgs e)
        {
            calculateTotal(null, null);
        }

        private void includeSC(object sender, RoutedEventArgs e)
        {
            calculateTotal(null, null);
        }
    }
}
