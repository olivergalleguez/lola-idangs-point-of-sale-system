﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LI_POS
{
    /// <summary>
    /// Interaction logic for LoggedInWindow.xaml
    /// </summary>
    public partial class LoggedInWindow : Window
    {
        public LoggedInWindow()
        {
            InitializeComponent();
        }

        public void showDashboard()
        {
            dashboardcontainer.Children.Clear();
            dashboardcontainer.Children.Add(new Dashboard());
        }

        public void showProductsPage()
        {
            dashboardcontainer.Children.Clear();
            dashboardcontainer.Children.Add(new ProductsPage());
        }

        public void ShowDashboardPage()
        {
            dashboardcontainer.Children.Clear();
            dashboardcontainer.Children.Add(new Dashboard());
        }

        public void ShowNewOrderPage()
        {
            dashboardcontainer.Children.Clear();
            dashboardcontainer.Children.Add(new TillPage());
        }

        public void ShowTransactionHistoryPage()
        {
            dashboardcontainer.Children.Clear();
            dashboardcontainer.Children.Add(new TransactionHistoryPage());
        }
    }
}
