﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LI_POS
{
    /// <summary>
    /// Interaction logic for QuantityInputBox.xaml
    /// </summary>
    public partial class QuantityInputBox : Window
    {
        private QuantityInputBox()
        {
            InitializeComponent();
        }

        private static int result = -1;
        private static QuantityInputBox inputBox;

        public static int Show()
        {
            inputBox = new QuantityInputBox();
            inputBox.quantity.Focus();
            inputBox.ShowDialog();
            
            return result;
        }

        private void closeEvent(object sender, System.ComponentModel.CancelEventArgs e)
        {
            result = -1;
        }

        private void closeWindow(object sender, RoutedEventArgs e)
        {
            result = Int32.Parse(inputBox.quantity.Text);
            this.Closing -= closeEvent;
            this.Close();
        }

        private void closeWindowNoValue(object sender, RoutedEventArgs e)
        {
            result = -1;
            this.Closing -= closeEvent;
            this.Close();
        }



        

        

    }
}
