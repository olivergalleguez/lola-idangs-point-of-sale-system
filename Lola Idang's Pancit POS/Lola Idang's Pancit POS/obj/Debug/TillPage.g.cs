﻿#pragma checksum "..\..\TillPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "2EF117A06F5B196BD26EBD7AEE1B14F4"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LI_POS {
    
    
    /// <summary>
    /// TillPage
    /// </summary>
    public partial class TillPage : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 15 "..\..\TillPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid DataGridProductList;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\TillPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid DataGridOrdersList;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\TillPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTotalItems;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\TillPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lblTotalAmount;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\TillPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtSearch;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\TillPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtAmountTendered;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\TillPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox chkSCDiscount;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LI_POS;component/tillpage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\TillPage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 13 "..\..\TillPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.toDashboardPage);
            
            #line default
            #line hidden
            return;
            case 2:
            this.DataGridProductList = ((System.Windows.Controls.DataGrid)(target));
            
            #line 15 "..\..\TillPage.xaml"
            this.DataGridProductList.AutoGeneratingColumn += new System.EventHandler<System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs>(this.renameProductListColumnHeaders);
            
            #line default
            #line hidden
            return;
            case 4:
            this.DataGridOrdersList = ((System.Windows.Controls.DataGrid)(target));
            
            #line 35 "..\..\TillPage.xaml"
            this.DataGridOrdersList.AutoGeneratingColumn += new System.EventHandler<System.Windows.Controls.DataGridAutoGeneratingColumnEventArgs>(this.renameOrderListColumnHeaders);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 57 "..\..\TillPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.checkOut);
            
            #line default
            #line hidden
            return;
            case 7:
            this.lblTotalItems = ((System.Windows.Controls.Label)(target));
            return;
            case 8:
            this.lblTotalAmount = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.txtSearch = ((System.Windows.Controls.TextBox)(target));
            
            #line 62 "..\..\TillPage.xaml"
            this.txtSearch.TextChanged += new System.Windows.Controls.TextChangedEventHandler(this.searchProductList);
            
            #line default
            #line hidden
            
            #line 62 "..\..\TillPage.xaml"
            this.txtSearch.KeyDown += new System.Windows.Input.KeyEventHandler(this.enterKey);
            
            #line default
            #line hidden
            return;
            case 10:
            this.txtAmountTendered = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.chkSCDiscount = ((System.Windows.Controls.CheckBox)(target));
            
            #line 66 "..\..\TillPage.xaml"
            this.chkSCDiscount.Unchecked += new System.Windows.RoutedEventHandler(this.removeSC);
            
            #line default
            #line hidden
            
            #line 66 "..\..\TillPage.xaml"
            this.chkSCDiscount.Checked += new System.Windows.RoutedEventHandler(this.includeSC);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            System.Windows.EventSetter eventSetter;
            switch (connectionId)
            {
            case 3:
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.Controls.Control.MouseDoubleClickEvent;
            
            #line 25 "..\..\TillPage.xaml"
            eventSetter.Handler = new System.Windows.Input.MouseButtonEventHandler(this.addProductToOrder);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            break;
            case 5:
            eventSetter = new System.Windows.EventSetter();
            eventSetter.Event = System.Windows.Controls.Control.MouseDoubleClickEvent;
            
            #line 45 "..\..\TillPage.xaml"
            eventSetter.Handler = new System.Windows.Input.MouseButtonEventHandler(this.removeProductToOrder);
            
            #line default
            #line hidden
            ((System.Windows.Style)(target)).Setters.Add(eventSetter);
            break;
            }
        }
    }
}

