﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LI_POS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Left = 0;
            this.Top = 0;
        }

        private void attemptLogin(object sender, RoutedEventArgs e)
        {
            lblLoginFailed.Visibility = System.Windows.Visibility.Hidden;
            using (var db = new LIPOSDbContext())
            {
                User testUser = (from user in db.Users
                                   where user.UserName == TxtUsername.Text && user.Password == txtPassword.Password
                                   select user).SingleOrDefault();

                if (testUser == null)
                {
                    lblLoginFailed.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    Session.ActiveUser = testUser;
                    this.Close();
                    Session.LoggedInWindow.Show();

                    // Insert dashboard usercontrol
                    Session.LoggedInWindow.showDashboard();
                }

            }
        }

        private void TxtUsername_TextChanged(object sender, TextChangedEventArgs e)
        {
            lblLoginFailed.Visibility = System.Windows.Visibility.Hidden;            
        }

        private void txtPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            lblLoginFailed.Visibility = System.Windows.Visibility.Hidden;
        }

    }
}
