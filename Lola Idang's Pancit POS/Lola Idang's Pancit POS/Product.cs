﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Data.Entity.Spatial;

namespace LI_POS
{
    [Table("Products")]
    public class Product
    {
        [Key]
        public int ProductID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Type { get; set; }
                
        public string Description { get; set; }

        [Required]
        public Decimal Price { get; set; }

        [Required]
        public Decimal CapitalPrice { get; set; }

        public string Image { get; set; }

        public string NameShortcut { get; set; }

        public int Locked { get; set; }
    }
}
