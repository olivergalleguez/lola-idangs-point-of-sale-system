﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LI_POS
{
    public class OrderedProduct
    {
        public int ProductID { get; set; }

        public string Name { get; set; }

        public string NameShortcut { get; set; }

        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public decimal SubtotalPrice { get; set; }

        public decimal SubtotalCapitalPrice { get; set; }

        public decimal CapitalPrice { get; set; }

        //public OrderedProduct(Product product, int quantity){
        //    ProductID = product.ProductID;
        //    this.Quantity = quantity;
        //    Price = product.Price;
        //    CapitalPrice = product.CapitalPrice;
        //    Name = product.Name;
        //    NameShortcut = product.NameShortcut;
            
        //    SubtotalPrice = Price * quantity;
        //    SubtotalCapitalPrice = CapitalPrice * quantity;
        //}

        public OrderedProduct()
        {
            // new from database
        }

       

    }
}
