﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Data.Entity.Spatial;

namespace LI_POS
{
    [Table("Transactions")]
    public class Transaction
    {
        [Key]
        public int TransactionID { get; set; }

        public string TransactionDate { get; set; }

        public string TransactionTime { get; set; }

        public string LoggedInEmployee { get; set; }

        public string Details { get; set; }

        public decimal Net { get; set; }

        public decimal VAT { get; set; }

        public decimal TotalCapitalPrice { get; set; }

        public decimal AmountReceived { get; set; }

        public decimal TotalPrice { get; set; }

        public int SeniorCitizenID { get; set; }

    }
}
