﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Data.Entity.Spatial;

namespace LI_POS
{
    [Table("Users")]
    public partial class User
    {
        [Key]
        public int UserID { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string UserName { get; set; }

        [Required]
        [StringLength(2147483647)]
        public string Password { get; set; }

        [Required]
        public string UserAccessLevel { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }
        
        [Required]
        public string StaffRole { get; set; }

        [Required]
        public string DateOfPartnership { get; set; }

        [Required]
        public string Sex { get; set; }

        public string ContactNumber { get; set; }

        public string EmailAddress { get; set; }
    }
}
