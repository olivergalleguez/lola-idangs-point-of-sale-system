﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LI_POS
{
    /// <summary>
    /// Interaction logic for SeniorCitizenForm.xaml
    /// </summary>
    public partial class SeniorCitizenForm : Window
    {
        private static SeniorCitizen SC;
        private static SeniorCitizenForm form;
        private SeniorCitizenForm()
        {
            InitializeComponent();
        }

        public static SeniorCitizen GetSC()
        {
            form = new SeniorCitizenForm();
            form.ShowDialog();

            return SC;
        }

        private void closeEvent(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SC = null;  
        }

        private void Register(object sender, RoutedEventArgs e)
        {
            SC = new SeniorCitizen()
            {
                SCNumber = txtSCID.Text,
                FirstName = txtFirstName.Text,
                LastName = txtLastName.Text,
                Address = txtAddress.Text,
                BirthDate = ((DateTime)dpBirthdate.SelectedDate).ToString("MM/dd/yyyy")
            };

            using (LIPOSDbContext db = new LIPOSDbContext())
            {
                db.SeniorCitizens.Add(SC);
                db.SaveChanges();
            }

            MessageBox.Show("Customer Added.", "New Entry", MessageBoxButton.OK, MessageBoxImage.Information);
            this.Closing -= closeEvent;
            this.Close();
        }

        private void closeWindowNoValue(object sender, RoutedEventArgs e)
        {
            SC = null;
            this.Closing -= closeEvent;
            this.Close();
        }

        private void searchSC(object sender, RoutedEventArgs e)
        {
            using (LIPOSDbContext db = new LIPOSDbContext())
            {
                SeniorCitizen searched = (from senior in db.SeniorCitizens
                                          where (senior.FirstName + " " + senior.LastName).Equals(txtSearchSC.Text, StringComparison.CurrentCultureIgnoreCase)
                                          select senior).SingleOrDefault();

                if (searched != null)
                {
                    MessageBox.Show(String.Format("Customer found.\n{0} {1}, Senior Citizen Number {2}", searched.FirstName, searched.LastName, searched.SCNumber), "Record Matched", MessageBoxButton.OK, MessageBoxImage.Information);
                    SC = searched;
                    this.Closing -= closeEvent;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Customer not in record. Proceed to register.", "No Results", MessageBoxButton.OK, MessageBoxImage.Information);
                    txtFirstName.IsEnabled = true;
                    txtLastName.IsEnabled = true;
                    txtAddress.IsEnabled = true;
                    txtSCID.IsEnabled = true;
                    dpBirthdate.IsEnabled = true;
                    btnRegister.IsEnabled = true;
                    txtSCID.Focus();
                }
            }
        }
    }
}
