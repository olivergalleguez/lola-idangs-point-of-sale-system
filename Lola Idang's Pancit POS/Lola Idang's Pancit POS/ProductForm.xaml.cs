﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LI_POS
{
    /// <summary>
    /// Interaction logic for NewProductForm.xaml
    /// </summary>
    public partial class ProductForm : Window
    {
        private static string imagepath = "";
        private static string base64img = "";

        private Product toSave = new Product();

        public ProductForm(FormMode mode, Product product)
        {
            InitializeComponent();
            toSave = product;
            switch (mode)
            {
                case FormMode.New:
                    header.Content = "NEW PRODUCT FORM";
                    this.Title = "New Product";

                    btnDelete.Visibility = Visibility.Hidden;
                    actionButton.Click += new RoutedEventHandler(saveNew);
                    break;
                case FormMode.View:
                    // all controls disabled
                    this.Title = "Product Information (Product ID:" + product.ProductID + ")";
                    header.Content = "PRODUCT INFORMATION";

                    txtCapPrice.Text = product.CapitalPrice.ToString();
                    txtDescription.Text = product.Description;
                    txtName.Text = product.Name;
                    txtNameShortcut.Text = product.NameShortcut;
                    txtPrice.Text = product.Price.ToString();
                    txtType.Text = product.Type.ToString();
                    if (product.Locked == 0)
                    {
                        chkLocked.IsChecked = false;
                    }
                    else
                    {
                        chkLocked.IsChecked = true;
                    }

                    if (product.Image != null && product.Image.Trim() != "")
                    {

                        // Clue: Save image to disk first then load to image control
                        // Save the location also
                        byte[] imagebytes = Convert.FromBase64String(product.Image);
                        BitmapImage bmp = new BitmapImage();
                        MemoryStream ms = new MemoryStream();
                        ms.Write(imagebytes, 0, imagebytes.Length);
                        ms.Position = 0;

                        bmp.StreamSource = ms;
                        bmp.CacheOption = BitmapCacheOption.OnLoad;
                        bmp.Freeze();

                        imgThumbnail.Source = bmp;

                        
                    }

                    actionButton.Content = "Edit";
                    actionButton.Click += new RoutedEventHandler(enableEdit);

                    browseImage.Visibility = Visibility.Hidden;
                    txtCapPrice.IsReadOnly = true;
                    txtDescription.IsReadOnly = true;
                    txtName.IsReadOnly = true;
                    txtNameShortcut.IsReadOnly = true;
                    txtPrice.IsReadOnly = true;
                    txtType.IsReadOnly = true;
                    chkLocked.IsEnabled = false;
                    break;
                case FormMode.Edit:
                    header.Content = "EDIT PRODUCT (Product ID:" + product.ProductID + ")" ;
                    this.Title = "Edit Product";

                    txtCapPrice.Text = product.CapitalPrice.ToString();
                    txtDescription.Text = product.Description;
                    txtName.Text = product.Name;
                    txtNameShortcut.Text = product.NameShortcut;
                    txtPrice.Text = product.Price.ToString();
                    txtType.Text = product.Type.ToString();if (product.Locked == 0)
                    {
                        chkLocked.IsChecked = false;
                    }
                    else
                    {
                        chkLocked.IsChecked = true;
                    }


                    actionButton.Click += new RoutedEventHandler(saveEdit);
                    break;
            }
        }

        private void saveNew(object sender, RoutedEventArgs e)
        {
            try
            {
                
                toSave.Name = txtName.Text;
                toSave.Description = txtDescription.Text;
                toSave.Type = txtType.Text;
                toSave.Price = Decimal.Parse(txtPrice.Text);
                toSave.CapitalPrice = Decimal.Parse(txtCapPrice.Text);
                toSave.NameShortcut = txtNameShortcut.Text;
                toSave.Image = "";
                if (chkLocked.IsChecked == true)
                {
                    toSave.Locked = 1;
                }
                else
                {
                    toSave.Locked = 0;
                }

                if (imagepath != "")
                {
                    byte[] imagebytes = File.ReadAllBytes(imagepath);
                    toSave.Image = Convert.ToBase64String(imagebytes);
                }

                using (LIPOSDbContext db = new LIPOSDbContext())
                {
                    db.Products.Add(toSave);
                    db.SaveChanges();
                }

                MessageBox.Show("Product Added.");
                this.Close();

                Session.RecentlyAddedProduct = toSave;
                
            }
            catch (FormatException fx)
            {
                MessageBox.Show("Invalid Format. \nNon-numerical values are not allowed on Price and Capital Price fields.", "Format Error");
            }
            catch (Exception ex)
            {
                MessageBox.Show("An error occured. \nYour numerical values might be too high or too low.\n\n" + ex.Message, "Error");
            }
        }

        private void saveEdit(object sender, RoutedEventArgs e)
        {
            using (LIPOSDbContext db = new LIPOSDbContext())
            {
                Product toEdit = (from product in db.Products where product.ProductID == toSave.ProductID select product).FirstOrDefault();
                toEdit.Name = txtName.Text;
                toEdit.Description = txtDescription.Text;
                toEdit.Type = txtType.Text;
                toEdit.Price = Decimal.Parse(txtPrice.Text);
                toEdit.CapitalPrice = Decimal.Parse(txtCapPrice.Text);
                toEdit.NameShortcut = txtNameShortcut.Text;
                toEdit.Image = "";
                if (chkLocked.IsChecked == true)
                {
                    toEdit.Locked = 1;
                }
                else
                {
                    toEdit.Locked = 0;
                }

                db.SaveChanges();

                Session.RecentlyModifiedProduct = toEdit;

                MessageBox.Show("Product Information successfully modified", "Success");
                this.Close();

            }
        }

        private void enableEdit(object sender, RoutedEventArgs e)
        {
            browseImage.Visibility = Visibility.Visible;
            txtCapPrice.IsReadOnly = false;
            txtDescription.IsReadOnly = false;
            txtName.IsReadOnly = false;
            txtNameShortcut.IsReadOnly = false;
            txtPrice.IsReadOnly = false;
            txtType.IsReadOnly = false;
            chkLocked.IsEnabled = true;

            btnDelete.Visibility = Visibility.Hidden;
            header.Content = "EDIT PRODUCT (Product ID:" + toSave.ProductID + ")";
            this.Title = "Edit Product";
            actionButton.Content = "Save";
            actionButton.Click -= enableEdit;
            actionButton.Click += new RoutedEventHandler(saveEdit);
        }

        private void showFilePicker(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog fp = new Microsoft.Win32.OpenFileDialog();
            fp.Filter = "Image files (.jpg)|*.jpg";
            fp.ShowDialog();

            imagepath = fp.FileName;
            if (imagepath != "")
            {
                Uri imageuri = new Uri(imagepath, UriKind.Absolute);
                BitmapImage img = new BitmapImage(imageuri);
                img.CacheOption = BitmapCacheOption.OnLoad;
                img.Freeze();
                imgThumbnail.Source = img;
            }
                        
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            if (toSave.Locked == 0)
            {
                MessageBoxResult dialogResult = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", MessageBoxButton.YesNo);

                if (dialogResult == MessageBoxResult.Yes)  
                {
                    using (var db = new LIPOSDbContext())
                    {
                        Product toDelete = db.Products.Where(x => x.ProductID == toSave.ProductID).FirstOrDefault();
                        db.Products.Remove(toDelete);
                        db.SaveChanges();

                        Session.RecentlyDeletedProduct = toSave.ProductID;
                        MessageBox.Show("Successfully deleted:\n\n" + toSave.Name, "Delete Success");
                        this.Close();
                    }

                }
                else
                {
                    
                }
            }
            else
            {
                MessageBox.Show("Cannot delete Product. Edit then uncheck the lock option first.", "Delete Restricted");
            }
        }
        

    }

    public enum FormMode { New, Edit, View};
}
