﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LI_POS
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : UserControl
    {
        ObservableCollection<Transaction> transactionHistoryList = new ObservableCollection<Transaction>();

        public Dashboard()
        {
            InitializeComponent();
        }

        private void openProductsPage(object sender, RoutedEventArgs e)
        {
            Session.LoggedInWindow.showProductsPage();
        }

        private void openNewOrderPage(object sender, RoutedEventArgs e)
        {
            Session.LoggedInWindow.ShowNewOrderPage();
        }

        private void showTransactionHistoryPage(object sender, RoutedEventArgs e)
        {
            Session.LoggedInWindow.ShowTransactionHistoryPage();
        }

    }
}
